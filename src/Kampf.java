public class Kampf {
    //protected heißt das er auf seine eigene Variablen zugreift so das man sie immer verwenden kann auch andere für sich selbst!
    // "Krieger kampfKrieger" heißt quasi das die Klasse Krieger in eine Variable kampfKrieger gepackt wird!
    // so kann man auf die Methoden und Objekte innerhalb der Klasse Krieger oder Magier zugreifen.
    protected Krieger kampfKrieger;
    protected Magier kampfMagier;

    public Kampf(Krieger kampfKrieger, Magier kampfMagier)
    {
        this.kampfKrieger = kampfKrieger;
        this.kampfMagier = kampfMagier;

    }

    public void macheSchaden()
    {
        int schadensberechnung;

        for(int i = 1; i <= 10; i++)
        {
            /**
            int magierkktuellesleben;
            magierkktuellesleben = kampfMagier.lebenspunkte - kampfKrieger.schwert.schaden;
            magierkktuellesleben = magierkktuellesleben - kampfKrieger.schwert.schaden;
            System.out.println(magierkktuellesleben);
            if(magierkktuellesleben <=0)
            {
                System.out.println("Der Magier " + kampfMagier.getName() + "ist tot!");
            }
             */
            int kriegerSchaden = this.kampfKrieger.schwert.getSchaden();
            this.kampfMagier.veringereLeben(kriegerSchaden);

            // kurze schreibweise
            // this.kampfMagier.veringereLeben(this.kampfKrieger.schwert.getSchaden());

            System.out.println("Krieger " + this.kampfKrieger.getName() + " macht " + kriegerSchaden + " an " + this.kampfMagier.getName() + "\n");

            if (this.kampfMagier.getLebenspunkte() <= 0) {
                System.out.println("Der Magier " + this.kampfMagier.getName() + " ist tot!");
            }

        }
    }

}
