public class Magier  {
        protected int lebenspunkte;
        protected String name;
        protected Schwert stab;


        public Magier(int lebenspunkte, String name)
        {
            this.lebenspunkte = 100;
            this.name = name;

        }
        // getter Methode die aus der Methode die Informationen herausbekommt "Get"
        public int getLebenspunkte() {
            return lebenspunkte;
        }

        public String getName() {

            return name;
        }

        public void setStab(Schwert stab) {
            this.stab = stab;
        }

        public void veringereLeben(int schaden) {
            this.lebenspunkte = this.lebenspunkte - schaden;
        }
}


